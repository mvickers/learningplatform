export interface Tab {
  ionIconName: string;
  tabTitle: string;
}
