export default interface Alert {
  alert: string;
  alertType: "login" | "lesson";
}
